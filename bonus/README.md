# P3: K3D basic example with ARGOCD and Gitlab self-hosted

## Implementation

- ArgoCD est isole dans le namespace `argocd`
- Le deploiement du playground est isole dans le namespace `dev`
- La configuration du deploiement du playground par argocd est effectuee
de maniere declarative. [argocd declarative setup](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)
- L'installation de gitlab est assure par `helm`

## Installation

L'installation de collections de roles Ansible est necessaire:

```bash
$ ansible-galaxy install -r scripts/ansible-requirements.yml
```

La connexion aux differents services gitlab se fait sous le nom de domaine example.com
- gitlab.example.com
- registry.example.com
- minio.example.com

Ces noms de domaines prives doivent etre enregistres. La creation d'un serveur DNS
est necessaire pour faire correspondre ces NDD a l'adresse du load balancer pointant
sur le cluster.
Les services a l'interieur du cluster ayant besoin d'acceder a gitlab par son nom de domaine
externe, la modification de la configuration de CoreDNS est aussi necessaire.

Des variables ansible permettent de configurer l'ensemble.

```yaml
# adresse du load balancer k3d (docker inspect id_du_container)
k3d_loadbalancer_addr: "172.23.0.2"

```

L'installation du cluster k3d et le deploiement des applications est automatise par ansible

```bash
$ ansible-playbook playbook.yml
```

## ArgoCD

L'acces externe au serveur argoCD se fait par ouverture d'un proxy ou
par forward d'un port.

```bash
kubectl port-forward -n argocd svc/argocd-server 8080:443
```

Lors de l'installation, un compte admninistrateur (`admin`) est cree
et un mot de passe est genere et stock dans un objet k8s de type `Secret`

Pour changer le mot de passe du compte admin facilement, ouvrez le fichier `group_vars/all.yml` et redefinissez la variable `argocd_password`.

Executez ensuite le playbook `update-argocd-password.yml`

```bash
$ ansible-playbook update-argocd-password.yml
```

## Gitlab

Gitlab est installe dans un namespace `gitlab`. Lors de l'installation, un compte
root est cree et un mot de passe par defaut lui est attribue. J'ai la flemme
d'automatiser le changement de mot de passe, bien qu'il soit possible de le faire
en passant par l'api de gitlab.
Pour recuperer le mot de passe, tapez la commande suivante:

```bash
$ kubectl get secret -n gitlab gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo
```

## Will playground server

Vous pouvez acceder au playground par le biais de l'url `http://will.example.com` (partant du principe que l'hote figure dans `/etc/hosts`) ou par l'utilitaire `curl`

```bash
curl -H "Host: will.example.com" localhost
```
