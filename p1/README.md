# P1: Deploy k3s Cluster

## Cluster provisioning

```txt
+------------------------------------------------------------------------------+
|     HOST                                                                     |
|                                                                              |
|                   +-------------+                          +---------------+ |
|  +---------+   ---| VBOX MANAGE |  Manage VM life cycle    | +-----------+ | |
|  | VAGRANT |--/   |             |--------------------------| | Master VM | | |
|  |         |--\   +-------------+                          | +-----------+ | |
|  +---------+   ---+-------------+ Manage OS configuration  | +-----------+ | |
|                   | ANSIBLE     |--------------------------| |Worker VM  | | |
|                   +-------------+                          | +-----------+ | |
|                                                            +---------------+ |
+------------------------------------------------------------------------------+
```

## Installation

### Pre-requis

- Vagrant
- Ansible

L'installation de collections de roles Ansible est necessaire:

```bash
$ ansible-galaxy install -r scripts/ansible-requirements.yml
```

### Deploiement

vagrant up

### Get kubeconfig

cp config/.../config ~/.kube/config