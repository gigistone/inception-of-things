# P2: K3S basic app deployment with web servers

## Installation

### Pre-requis

- Vagrant
- Ansible

L'installation de collections de roles Ansible est necessaire:

```bash
$ ansible-galaxy install -r scripts/ansible-requirements.yml
```

### Deploiement

vagrant up

### Get kubeconfig

cp config/.../config ~/.kube/config

