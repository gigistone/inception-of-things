# P3: K3D basic example with ARGOCD

## Implementation

- ArgoCD est isole dans le namespace `argocd`
- Le deploiement du playground est isole dans le namespace `dev`
- La configuration du deploiement du playground par argocd est effectuee
de maniere declarative. [argocd declarative setup](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)

## Installation

L'installation du cluster k3d et le deploiement des applications est automatise par ansible

```bash
$ ansible-playbook deploy-willapp.yml
```

## ArgoCD

L'acces externe au serveur argoCD se fait par ouverture d'un proxy ou
par forward d'un port.

```bash
kubectl port-forward -n argocd svc/argocd-server 8080:443
```

Lors de l'installation, un compte admninistrateur (`admin`) est cree
et un mot de passe est genere et stock dans un objet k8s de type `Secret`

Pour changer le mot de passe du compte admin facilement, ouvrez le fichier `group_vars/all.yml` et redefinissez la variable `argocd_password`.

Executez ensuite le playbook `update-argocd-password.yml`

```bash
$ ansible-playbook update-argocd-password.yml
```

## Will playground server

Vous pouvez acceder au playground par le biais de l'url `http://will.example.com` (partant du principe que l'hote figure dans `/etc/hosts`) ou par l'utilitaire `curl`

```bash
curl -H "Host: will.example.com" localhost
```
